<?php

namespace App;
use Google_Client; 
use Google_Service_Analytics; 

class GoogleApi{

  /*
  * Run everything
  */
  public function  __construct(){
    $analytics = $this->initializeAnalytics();
    $profile   = $this->getFirstProfileId($analytics);
  }
  
  /*
  * Creates and returns the Analytics Reporting service object.
  */
  protected function initializeAnalytics(){
    $public_path = rtrim(app()->basePath('public/'), '/');
    $KEY_FILE_LOCATION = $public_path.'/CCE-PortalWeb-bde83ee2b58c.json';
    $client = new Google_Client();
    $client->setApplicationName("Hello Analytics Reporting");
    $client->setAuthConfig($KEY_FILE_LOCATION);
    $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']); 
    $analytics = new Google_Service_Analytics($client);
    return $analytics;
  }

  /*
  * Get the user's first view (profile) ID.
  */
  protected function getFirstProfileId($analytics) {
    $accounts = $analytics->management_accounts->listManagementAccounts();
    if (count($accounts->getItems()) > 0) {
      $items = $accounts->getItems();
      $firstAccountId = $items[0]->getId();
      $properties = $analytics->management_webproperties
          ->listManagementWebproperties($firstAccountId);
      if (count($properties->getItems()) > 0) {
        $items = $properties->getItems();
        $firstPropertyId = $items[0]->getId();
        $profiles = $analytics->management_profiles
            ->listManagementProfiles($firstAccountId, $firstPropertyId);
        if (count($profiles->getItems()) > 0) {
          $items = $profiles->getItems();
          return $items[0]->getId();
        } else {
          throw new Exception('No views (profiles) found for this user.');
        }
      } else {
        throw new Exception('No properties found for this user.');
      }
    } else {
      throw new Exception('No accounts found for this user.');
    }
  }

  /*
  * Get the authentication token
  */
  public function getToken() {
    $public_path = rtrim(app()->basePath('public/'), '/');
    $KEY_FILE_LOCATION = $public_path.'/CCE-PortalWeb-bde83ee2b58c.json';
    $client = new Google_Client();
    $client->setApplicationName("Hello Analytics Reporting");
    $client->setAuthConfig($KEY_FILE_LOCATION);
    $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
    $client->refreshTokenWithAssertion();
    $token = $client->getAccessToken();
    $token = $token["access_token"];
    return $token;
  }
}
