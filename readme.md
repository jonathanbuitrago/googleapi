## Google api connection

Google api connection for reporting project

## Serving Your Application

php -S localhost:8000 -t public


## Connect and get token

http://localhost:8000/connect

Response format:

{"token":"ya29.ElnxA1WmL5-zbF_6fr7j3VrJvDoLWA8OKE4Gv8kfGI9kYD7tgb9jbfX-Z_us1ZjMF1_cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"}

### License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
